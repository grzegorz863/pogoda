﻿using System;
using System.IO;
using System.Net;
using System.Xml;
namespace Pogoda
{
    class IOstreamHandling
    {
        private string pathToFile = "C:\\ProgramData\\PlikiPogody\\data.xml";
        private string pathToMainFolder = "C:\\ProgramData\\PlikiPogody";
        private string pathToTempFolder = "C:\\ProgramData\\PlikiPogody\\temp";
		private string linkToMeteogramLegend = @"http://www.meteo.pl/um/metco/leg_um_pl_cbase_256.png";
		private string pathToMeteogramLegend = "C:\\ProgramData\\PlikiPogody\\legend.png";

		public string PathToFile
        {
            get
            {
                return pathToFile;
            }
        }

        public string SciezkaDoFolderu
        {
            get
            {
                return pathToMainFolder;
            }
        }

        protected virtual void CreateNewFolderIfDoesNotExist()
        {
            if (!Directory.Exists(pathToTempFolder))
            {
                Directory.CreateDirectory(pathToTempFolder);
            }
        }

        protected virtual void CreateNewFileIfDoesNotExist()
        {
			if (!File.Exists(pathToFile))
			{
				XmlDocument xmlDocument = new XmlDocument();
				XmlNode xmlNode = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
				xmlDocument.AppendChild(xmlNode);

				xmlNode = xmlDocument.CreateElement("localizations");
				xmlDocument.AppendChild(xmlNode);
				xmlDocument.Save(pathToFile);
			}
        }

		protected virtual void DownloadLegendIfDoesNotExist()
		{
			if(!File.Exists(pathToMeteogramLegend))
			{
				new WebClient().DownloadFile(linkToMeteogramLegend, pathToMeteogramLegend);
			}
		}

        public virtual void DeleteFilesFromTempFolder()
        {
			if (Directory.Exists(pathToTempFolder))
				Array.ForEach(Directory.GetFiles(pathToTempFolder), File.Delete);
        }
    }
}
