﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
namespace Pogoda
{
    class WrongCityException : System.Exception
    {
        public WrongCityException()
        {

        }

        public void Message()
        {
            MessageBox.Show("Blednie podana nazwa miasta", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
