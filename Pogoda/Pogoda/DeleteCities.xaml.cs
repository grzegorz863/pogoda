﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pogoda
{
    /// <summary>
    /// Interaction logic for UsuwanieMiasta.xaml
    /// </summary>
    public partial class DeleteCity : Window
    {
        List<City> listOfCities = new List<City>();

        public DeleteCity()
        {
            InitializeComponent();
            
            ((ListOfCities)Application.Current.MainWindow.DataContext).PListOfCities.ForEach((item)=> { listOfCities.Add((City)item.Clone()); });
            foreach (City m in listOfCities)
            {
                ListBoxItem item2 = new ListBoxItem();
                item2.Content = m.CityName;
                listBox.Items.Add(item2);
            }
        }

        private void B_Delete_Click(object sender, RoutedEventArgs e)
        {
            ListOfCities lista = new ListOfCities();
            lista.PListOfCities = listOfCities;
            ListBoxItem item = new ListBoxItem();
            item = (ListBoxItem)listBox.SelectedItem;
            try
            {
                listOfCities.Remove(lista.GetElement((string)item.Content));
                listOfCities = lista.PListOfCities;
                listBox.Items.Remove(listBox.SelectedItem);
            }
            catch
            {
                MessageBox.Show("Niezaznaczono żadnego miasta", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void B_Cancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Czy napewno chcesz wyjść? Usuwanie nie zostanie zapisane.", "Ostrzeżenie", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                this.DataContext = null;
                this.Close();
            }
        }

        private void B_OK_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = listOfCities;
            this.Close();
        }

    }
}
