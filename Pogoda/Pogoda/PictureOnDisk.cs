﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Pogoda
{
    class PictureOnDisk
    {
        int fileNumber = 0;

        public PictureOnDisk()
        {

        }

        public Uri DownloadPicture(Link meteogramAddress)
        {
            WebClient wc = new WebClient();

            string pathToFileOnDisk = "C:\\ProgramData\\PlikiPogody\\temp\\pog" + fileNumber.ToString() + ".png";
            fileNumber++;
            wc.DownloadFile(meteogramAddress.ToString(), pathToFileOnDisk);
            return new Uri(pathToFileOnDisk, UriKind.Absolute);
        }

        public int FileNumber
        {
            set
            {
                this.fileNumber = value;
            }

            get
            {
                return this.fileNumber;
            }
        }
    }
}
