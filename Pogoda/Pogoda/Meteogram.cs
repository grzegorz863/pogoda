﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.IO;

namespace Pogoda
{
    class Meteogram : FileHandling
    {

        Link meteogramAddress;
        Uri pathToMeteogramOnDisk;
        PictureOnDisk picture; 
        City city;
       
        public Meteogram(City _city, PictureOnDisk _picture)
        {
            this.meteogramAddress = new Link(_city);
            this.city = _city;
            this.picture = _picture;
        }

        public Uri PathToMeteogramOnDisk
        {
            get
            {
                return this.pathToMeteogramOnDisk;
            }
        }

        public int NumberOfFileOnDisk
        {
            get
            {
                return picture.FileNumber;
            }
        }

        public string DateToShow
        {
            get
            {
                return this.PrepareDateToShow(this.meteogramAddress.AdjustedDate);
            }
        }

        public override void ExecuteTransfer() //pobiera meteogram na dysk komputera
        {
            this.pathToMeteogramOnDisk = this.picture.DownloadPicture(meteogramAddress);
            
            if (!this.IsMeteogramAvailable())
                this.CorrectMeteogram();
        }

        private string PrepareDateToShow(string dateToPrepare)
        {
            dateToPrepare = dateToPrepare.Insert(4, "-");
            dateToPrepare = dateToPrepare.Insert(7, "-");
            dateToPrepare = dateToPrepare.Insert(10, "   ");
            dateToPrepare += ":00:00";
            return dateToPrepare;
        }

        private bool IsMeteogramAvailable()
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = this.pathToMeteogramOnDisk;
            bitmap.EndInit();

            if ((bitmap.PixelHeight == 10) && (bitmap.PixelWidth == 10))
                return false;
            return true;
        }

        private void CorrectMeteogram()
        {
            meteogramAddress.CorrectURL();
            this.ExecuteTransfer();
        }
    }
}
