﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Pogoda
{
    class City : ICloneable
    {
        private string name;
        private int row;
        private int col;

        public City(string _name, int _row, int _col)
        {
            this.name = _name;
            this.row = _row;
            this.col = _col;
        }

        public City(string _name, string _row, string _col)
        {
            this.CheckName(_name);
            this.CheckCoordinates(_row, _col);
            this.name = _name;
            this.row = int.Parse(_row);
            this.col = int.Parse(_col);
        }

        private void CheckName(string nazwa)
        {
            if (nazwa=="")
				throw new WrongCityException();
        }

        object ICloneable.Clone()
        {
            City newCity = new City(this.name, this.row, this.col);
            return  (object)newCity;
        }

        public object Clone()
        {
            City newCity = new City(this.name, this.row, this.col);
            return (object)newCity;
        }

        public string CityName
        {
            get
            {
                return name;
            }
        }

        public int Row
        {
            get
            {
                return row;
            }
        }

        public int Col
        {
            get
            {
                return col;
            }
        }

        public override string ToString()
        {
            return this.CityName + " " + this.Row.ToString() + " " + this.Col.ToString();
        }

        private void CheckCoordinates(string row, string col)
        {
            int temp = 0;

            if (int.TryParse(row, out temp))
            {
                int test_row = int.Parse(row);
                if ((test_row > 600) || (test_row < 0))
                    throw new WrongCoordinatesException();
            }
            else
                throw new WrongCoordinatesException();

            if (int.TryParse(col, out temp))
            {
                int test_col = int.Parse(col);
                if ((test_col > 600) || (test_col < 0))
                    throw new WrongCoordinatesException();
            }
            else
                throw new WrongCoordinatesException();
        }
    }
}
