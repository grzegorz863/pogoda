﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Drawing;

namespace Pogoda
{
	public partial class MainWindow : Window
	{
		ListOfCities listOfCities = new ListOfCities();
		Meteogram meteogram;
		PictureOnDisk picture = new PictureOnDisk();
		AddingCity addition;
		DeleteCity delete;
		const int FIRST_ELEMENT = 0;
		bool trigger = false;
		bool listOfCitiesIsChanged = false;

		public MainWindow()
		{
			InitializeComponent();

			ReadFromFile readFromFile = new ReadFromFile();
			readFromFile.DeleteFilesFromTempFolder();
			readFromFile.ExecuteTransfer();
			listOfCities.PListOfCities = readFromFile.ListOfCities;
			if(listOfCities.IsEmpty)
			{
				this.EmptyListHandling();
			}
			AddToComboBox();
		}

		private void MainWindow_Closed(object sender, EventArgs e)
		{
			if (listOfCitiesIsChanged)
			{
				SaveToFile saveToFile = new SaveToFile(listOfCities);
				saveToFile.ExecuteTransfer();
			}

			if (delete != null)
				delete.Close();
			if (addition != null)
				addition.Close();
		}

		private void EmptyListHandling()
		{
			MessageBox.Show("Nie masz dodanego żadnego miasta. Dodaj swoje pierwsze miasto!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void AddToComboBox()
		{
			for (int i = 0; i < listOfCities.NumberOfElements; i++)
				comboBox.Items.Add(listOfCities.GetElement(i).CityName);
			if (!listOfCities.IsEmpty)
				comboBox.SelectedItem = comboBox.Items.GetItemAt(0);
		}

		private void PrepareMeteogram()
		{
			meteogram.ExecuteTransfer();

			BitmapImage bitmap = new BitmapImage();
			bitmap.BeginInit();
			bitmap.UriSource = this.meteogram.PathToMeteogramOnDisk;
			this.picture.FileNumber = this.meteogram.NumberOfFileOnDisk;
			bitmap.EndInit();
			if (bitmap.PixelWidth == 540)
				this.Width = 577;
			image.Source = bitmap;
		}

		private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!trigger)
			{
				string cityName = "";
				City city;
				cityName = comboBox.SelectedItem.ToString();
				city = listOfCities.GetElement(cityName);
				meteogram = new Meteogram(city, picture);
				this.PrepareMeteogram();
				TB_measurement_date.Text = meteogram.DateToShow;
			}
		}

		private void B_addition_Click(object sender, RoutedEventArgs e)
		{
			addition = new AddingCity(picture);
			this.IsEnabled = false;
			addition.Show();
			addition.Closed += Addition_Closed;
		}

		private void B_delete_Click(object sender, RoutedEventArgs e)
		{
			trigger = true;
			this.DataContext = listOfCities;
			delete = new DeleteCity();
			delete.Show();
			this.IsEnabled = false;
			delete.Closed += Delete_Closed;
			trigger = false;
		}

		private void Delete_Closed(object sender, EventArgs e)
		{
			this.IsEnabled = true;
			List<City> l = new List<City>();
			DeleteCity window = (DeleteCity)sender;
			l = (List<City>)window.DataContext;

			if (l != null)
			{
				listOfCities.PListOfCities = l;
				trigger = true;
				comboBox.Items.Clear();
				this.AddToComboBox();
			}
			trigger = false;
			delete = null;
			listOfCitiesIsChanged = true;
		}

		private void Addition_Closed(object sender, EventArgs e) //zamknięcie okna dodawania miast
		{
			bool emptyList = this.listOfCities.IsEmpty;
			List<City> l = new List<City>();
			AddingCity win = (AddingCity)sender;
			l = (List<City>)win.DataContext;

			if ((l != null) && (l.Count > 0))
			{
				listOfCities.MergeTwoListsOfCities(l);
				trigger = true;
				comboBox.Items.Clear();
				this.AddToComboBox();
			}
			trigger = false;
			addition = null;
			this.IsEnabled = true;
			listOfCitiesIsChanged = true;
			if (emptyList)
			{
				System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
				Application.Current.Shutdown();
			}
		}

		
		private void CB_legend_click(object sender, RoutedEventArgs e)
		{
			if(CB_legend.IsChecked.Value)
			{
				this.Width += 290;
				imageLegend.Visibility = Visibility.Visible;
				imageLegend.Source = new BitmapImage(new Uri("C:\\ProgramData\\PlikiPogody\\legend.png"));
			}
			else
			{
				this.Width -= 290;
				imageLegend.Visibility = Visibility.Hidden;
			}
		}
	}
}
