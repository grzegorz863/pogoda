﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pogoda
{
    class ListOfCities
    {
        private List<City> listOfCities = new List<City>();

        public ListOfCities()
        {

        }
        
        public List<City> PListOfCities
        {
            set
            {
                listOfCities = value;
            }

            get
            {
                return listOfCities;
            }
        }
        
        public int NumberOfElements
        {
            get
            {
                return listOfCities.Count;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return !listOfCities.Any();
            }
        }

        public City GetElement(int index)
        {
            return listOfCities[index];
        }

        public City GetElement(string name)
        {
            for(int i = 0;i<listOfCities.Count;i++)
            {
                if(listOfCities[i].CityName == name)
                {
                    return listOfCities[i];
                }
            }
            return null;
        }

        public void MergeTwoListsOfCities(List<City> l)
        {
            listOfCities.AddRange(l);
        }

        
        public void Add(City _m)
        {
            listOfCities.Add(_m);
        }

        public void DeleteList()
        {
            listOfCities.Clear();
        }
    }
}
