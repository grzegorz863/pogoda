﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pogoda
{
    /// <summary>
    /// Interaction logic for DodawanieMiasta.xaml
    /// </summary>
    public partial class AddingCity : Window
    {
        List<City> listWithNewCities = new List<City>();
		PictureOnDisk picture;
        public AddingCity(object data)
        {
            InitializeComponent();
			this.picture = (PictureOnDisk)data;
        }
        
        private void B_Cancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Czy napewno chcesz wyjść? Dodane miasta zostaną utracone.", "Ostrzeżenie", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                this.listWithNewCities.Clear();
                this.DataContext = listWithNewCities;
                this.Close();
            }
        }

        private void B_Addition_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                City newCity = new City(TB_city_name.Text, TB_row.Text, TB_col.Text);
				Link linkToPicture = new Link(newCity);
				Uri address = picture.DownloadPicture(linkToPicture);
				this.CityExist(address, linkToPicture);

				listWithNewCities.Add(newCity);

                TB_city_name.Clear();
                TB_col.Clear();
                TB_row.Clear();
            }
            catch(WrongCityException error)
            {
                error.Message();
                TB_city_name.Clear();
            }
            catch(WrongCoordinatesException error)
            {
                error.Message();
                TB_col.Clear();
                TB_row.Clear();
            }
        }

		private void CityExist(Uri pathOnDisk, Link link)
		{
			BitmapImage bitmap = new BitmapImage();
			bitmap.BeginInit();
			bitmap.UriSource = pathOnDisk;
			bitmap.EndInit();

			if ((bitmap.PixelHeight == 10) && (bitmap.PixelWidth == 10))
				link.CorrectURL();

			bitmap = null;

			pathOnDisk = picture.DownloadPicture(link);
			BitmapImage bitmap2 = new BitmapImage();
			bitmap2.BeginInit();
			bitmap2.UriSource = pathOnDisk;
			bitmap2.EndInit();

			if ((bitmap2.PixelHeight == 30) && (bitmap2.PixelWidth == 550))
				throw new WrongCoordinatesException();

			bitmap2 = null;
		}

        private void B_OK_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = listWithNewCities;
            this.Close();
        }
    }
}
