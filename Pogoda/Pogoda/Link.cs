﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Drawing;

namespace Pogoda
{
    class Link
    {
        private string linkText = @"http://www.meteo.pl/um/metco/mgram_pict.php?ntype=0u&fdate=&row=&col=&lang=pl";
        private City city;
        DateTime currentDate;
        string adjustedDate;

        public Link(City _city)
        {
            this.city = _city;
            this.currentDate = DateTime.Now;
            this.GenerateURL();
        }

        public string AdjustedDate
        {
            get
            {
                return this.adjustedDate;
            }
        }

        public override string ToString()
        {
            return linkText;
        }

        private void GenerateURL()
        {
            int dateIndex =  linkText.IndexOf("date=") + "date=".Length;
            string date = currentDate.Year.ToString() + this.AdjustMonth(currentDate.Month) + this.AdjustDay(currentDate.Day) + this.AdjustHour(currentDate.Hour);
            linkText = linkText.Insert(dateIndex, date);

            int row_index = linkText.IndexOf("row=") + "row=".Length;
            string row = city.Row.ToString();
            linkText = linkText.Insert(row_index, row);
            
            int col_index = linkText.IndexOf("col=") + "col=".Length;
            string col = city.Col.ToString();
            linkText = linkText.Insert(col_index, col);

            this.adjustedDate = date;
        }

        public void CorrectURL()
        {
            this.currentDate = this.currentDate.AddHours(-6);
            int dateIndex = linkText.IndexOf("date=") + "date=".Length;
            string date = currentDate.Year.ToString() + this.AdjustMonth(currentDate.Month) + this.AdjustDay(currentDate.Day) + this.AdjustHour(currentDate.Hour);
            linkText = linkText.Remove(dateIndex, 10);
            linkText = linkText.Insert(dateIndex, date);

            this.adjustedDate = date;
        }
        
        private string AdjustMonth(int m)
        {
            if(m<10)
            {
                return "0" + m.ToString();
            }
            else
            {
                return m.ToString();
            }
        }

        private string AdjustDay(int d)
        {
            if (d < 10)
            {
                return "0" + d.ToString();
            }
            else
            {
                return d.ToString();
            }
        }

        private string AdjustHour(int h)
        {
            if(h<=6)
            {
                return "00";
            }
            else
            {
                if(h<=12)
                {
                    return "06";
                }
                else
                {
                    if(h<=18)
                    {
                        return "12";
                    }
                    else
                    {
                        return "18";
                    }
                }
            }
        }
    }
}
