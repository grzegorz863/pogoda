﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Pogoda
{
    class WrongCoordinatesException : System.Exception
    {
        public WrongCoordinatesException()
        {

        }

        public void Message()
        {
            MessageBox.Show("Blednie podane współrzędne miasta", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
