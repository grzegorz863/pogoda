﻿namespace Pogoda
{
    class FileHandling : IOstreamHandling
    {
        public virtual void ExecuteTransfer()
        {
            this.CreateNewFolderIfDoesNotExist();
            this.CreateNewFileIfDoesNotExist();
			this.DownloadLegendIfDoesNotExist();
        }

        protected sealed override void CreateNewFolderIfDoesNotExist()
        {
            base.CreateNewFolderIfDoesNotExist();
        }

        protected sealed override void CreateNewFileIfDoesNotExist()
        {
            base.CreateNewFileIfDoesNotExist();
        }

		protected sealed override void DownloadLegendIfDoesNotExist()
		{
			base.DownloadLegendIfDoesNotExist();
		}
	}
}
