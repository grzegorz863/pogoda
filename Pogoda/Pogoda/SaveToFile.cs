﻿using System.Xml;

namespace Pogoda
{
    class SaveToFile : FileHandling
    {
        ListOfCities listWithCitiesToSave;

        public SaveToFile(ListOfCities _listWithCitiesToSave)
        {
            this.listWithCitiesToSave = _listWithCitiesToSave;
        }

        public override void ExecuteTransfer()
        {
            base.ExecuteTransfer();

			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(this.PathToFile);
			XmlNodeList localizationsNodes = xmlDocument.GetElementsByTagName("localizations");
			XmlNode localizationsNode = localizationsNodes.Item(0);
			localizationsNode.RemoveAll();
			foreach (City city in listWithCitiesToSave.PListOfCities)
			{
				XmlNode cityNode = xmlDocument.CreateElement("city");
				XmlAttribute xmlAttribute = xmlDocument.CreateAttribute("name");
				xmlAttribute.Value = city.CityName;
				cityNode.Attributes.Append(xmlAttribute);
				XmlNode colNode = xmlDocument.CreateElement("col");
				colNode.InnerText = city.Col.ToString();
				XmlNode rowNode = xmlDocument.CreateElement("row");
				rowNode.InnerText = city.Row.ToString();
				cityNode.AppendChild(colNode);
				cityNode.AppendChild(rowNode);
				localizationsNode.AppendChild(cityNode);
			}
			xmlDocument.Save(this.PathToFile);
        }
    }
}
