﻿using System.Collections.Generic;
using System.Xml;

namespace Pogoda
{
    class ReadFromFile : FileHandling
    {
        List<City> listOfCities = new List<City>();

        public List<City> ListOfCities
        {
            get
            {
                return listOfCities;
            }
        }

        public override void ExecuteTransfer()
        {
            base.ExecuteTransfer();

			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(this.PathToFile);
			
			XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName("city");
			foreach (XmlNode xn in xmlNodeList)
			{
				if (xn.HasChildNodes)
				{
					string name = xn.Attributes.GetNamedItem("name").InnerText;
					string col = xn.ChildNodes.Item(0).InnerText;
					string row = xn.ChildNodes.Item(1).InnerText;
					City city = new City(name, row, col);
					listOfCities.Add(city);
				}
			}
		}

        public override void DeleteFilesFromTempFolder()
        {
            base.DeleteFilesFromTempFolder();
        }
    }
}
